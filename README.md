# drg_st_1_lvgl

## 介绍

* Makerbase DRG ST-1 STM32F407VET6核心板的LVGL案例库，配套B站“高博士-嵌入式”的“LVGL极速入门”视频教程的ppt课件和源码共享。
* 视频链接： <https://www.bilibili.com/video/BV1eh4y167JT/?share_source=copy_web&vd_source=a373179ac4c36034087312dd6ba63509>

## 内容简介

|名称|简介|
|--|--|
|1 Timer Interrupt        | 定时器中断案例
|2 Touchpad               | 触摸屏案例
|3 Lvgl Porting           | lvgl移植案例
|4 Lvgl Porting - template| lvgl移植添加了src文件到group的中间结果
|lvgl-release-v8.3裁剪后   | lvgl8.3裁剪之后可以直接用于移植的

## 硬件平台

* DRG ST-1 STM32F407VET6核心板+2.8寸TFT触摸屏
* 链接：<https://makerbase.taobao.com>

## 联系方式

|   |  |
|--:|:--|
| Q群：      |366182133|
| B站：      |<https://space.bilibili.com/486637340>
| 官方网站：  |<https://www.genbotter.com>
| 开发板购买：| <https://makerbase.taobao.com>
| 微信公众号：| GenBotter
