/**
 ****************************************************************************************************
 * @file        main.c
 * @author      Mr.Mr.
 * @version     V1.0
 * @date        2023-04-23
 * @brief       触摸屏实验
 * @license     Copyright (c) 2020-2032,  
 ****************************************************************************************************
 * @attention
 * 
 * 实验平台:ST-1 STM32F407核心板
 *  
 *  
 * 公司网址:www.genbotter.com
 * 购买地址:makerbase.taobao.com
 * 
 ****************************************************************************************************
 */

#include "./SYSTEM/sys/sys.h"
#include "./SYSTEM/delay/delay.h"
#include "./SYSTEM/usart/usart.h"
#include "./BSP/LED/led.h"
#include "./BSP/KEY/key.h"
#include "./BSP/LCD/lcd.h"
#include "./BSP/TOUCH/touch.h"
#include "./BSP/SRAM/sram.h"
#include "./BSP/TIMER/btim.h"

/**
 * @brief   清空屏幕并在右上角显示"RST"
 * @param   无
 * @retval  无
 */
static void load_draw_dialog(void)
{
    lcd_clear(WHITE);                                                /* 清屏 */
    lcd_show_string(lcddev.width - 24, 0, 200, 16, 16, "RST", BLUE); /* 显示清屏区域 */
}

/**
 * @brief   画粗线
 * @param   x1: 起点X坐标
 * @param   y1: 起点Y坐标
 * @param   x2: 终点X坐标
 * @param   y2: 终点Y坐标
 * @param   size: 线条粗细程度
 * @param   color: 线的颜色
 * @retval  无
 */
static void lcd_draw_bline(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint8_t size, uint16_t color)
{
    uint16_t t;
    int xerr = 0;
    int yerr = 0;
    int delta_x;
    int delta_y;
    int distance;
    int incx, incy, row, col;
    
    if ((x1 < size) || (x2 < size) || (y1 < size) || (y2 < size))
    {
        return;
    }
    
    delta_x = x2 - x1;                          /* 计算坐标增量 */
    delta_y = y2 - y1;
    row = x1;
    col = y1;
    if (delta_x > 0)
    {
        incx = 1;                               /* 设置单步方向 */
    }
    else if (delta_x == 0)
    {
        incx = 0;                               /* 垂直线 */
    }
    else
    {
        incx = -1;
        delta_x = -delta_x;
    }
    
    if (delta_y > 0)
    {
        incy = 1;
    }
    else if (delta_y == 0)
    {
        incy = 0;                               /* 水平线 */
    }
    else
    {
        incy = -1;
        delta_y = -delta_y;
    }
    
    if (delta_x > delta_y)
    {
        distance = delta_x;                     /* 选取基本增量坐标轴 */
    }
    else
    {
        distance = delta_y;
    }
    
    for (t=0; t<=(distance + 1); t++)           /* 画线输出 */
    {
        lcd_fill_circle(row, col, size, color); /* 画点 */
        xerr += delta_x;
        yerr += delta_y;
        
        if (xerr > distance)
        {
            xerr -= distance;
            row += incx;
        }
        
        if (yerr > distance)
        {
            yerr -= distance;
            col += incy;
        }
    }
}

/**
 * @brief   电阻触摸屏测试
 * @param   无
 * @retval  无
 */
void rtp_test(void)
{
    uint8_t key;
    uint8_t i = 0;
    
    while (1)
    {
        key = key_scan(0);
        tp_dev.scan(0);
        
        if (tp_dev.sta & TP_PRES_DOWN)                                          /* 触摸屏被按下 */
        {
            if ((tp_dev.x[0] < lcddev.width) && (tp_dev.y[0] < lcddev.height))
            {
                if ((tp_dev.x[0] > (lcddev.width - 24)) && (tp_dev.y[0] < 16))
                {
                    load_draw_dialog();                                         /* 清除 */
                }
                else
                {
                    tp_draw_big_point(tp_dev.x[0], tp_dev.y[0], RED);           /* 画点 */
                }
            }
        }
        else
        {
            delay_ms(10);                                                       /* 没有按键按下的时候 */
        }
        
        if (key == KEY0_PRES)                                                   /* KEY0按下，则执行校准程序 */
        {
            tp_adjust();                                                        /* 屏幕校准 */
            tp_save_adjust_data();
            load_draw_dialog();
        }
        
        i++;
        if ((i % 20) == 0)
        {
            LED2_TOGGLE();
        }
    }
}

/* 10个触控点的颜色（电容触摸屏用） */
static const uint16_t POINT_COLOR_TBL[10] = {RED, GREEN, BLUE, BROWN, YELLOW,
                                             MAGENTA, CYAN, LIGHTBLUE, BRRED, GRAY};

int main(void)
{
    HAL_Init();                         /* 初始化HAL库 */
    sys_stm32_clock_init(336, 8, 2, 7); /* 配置时钟，168MHz */
    delay_init(168);                    /* 初始化延时 */
    usart_init(115200);                 /* 初始化串口 */
    led_init();                         /* 初始化LED */
    key_init();                         /* 初始化按键 */
    sram_init();
    lcd_init();                         /* 初始化LCD */
    tp_dev.init();                      /* 初始化触摸屏 */
    
    btim_timx_int_init(10000 - 1, 8400);
    
    lcd_show_string(30, 50, 200, 16, 16, "STM32", RED);
    lcd_show_string(30, 70, 200, 16, 16, "TOUCH TEST", RED);
    lcd_show_string(30, 90, 200, 16, 16, "ATOM@ALIENTEK", RED);
    
    /* 电阻屏显示触摸校准提示 */
    if ((tp_dev.touchtype & 0x80) == 0)
    {
        lcd_show_string(30, 110, 200, 16, 16, "Press KEY0 to Adjust", RED);
    }
    delay_ms(1500);
    load_draw_dialog();
    
    if ((tp_dev.touchtype & 0x80) == 0)
    {
        /* 电阻屏测试 */
        rtp_test();
    }
}
