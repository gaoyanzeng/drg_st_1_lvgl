/**
 ****************************************************************************************************
 * @file        main.c
 * @author      小丁
 * @version     V1.0
 * @date        2023-04-23
 * @brief       基本定时器中断实验
 ****************************************************************************************************
 * @attention
 * 
 * 适配开发板:DRG STM32F407VET6开发板
 * 购买网址：https://makerbase.taobao.com/
 * 
 ****************************************************************************************************
 */

#include "./SYSTEM/sys/sys.h"
#include "./SYSTEM/delay/delay.h"
#include "./SYSTEM/usart/usart.h"
#include "./BSP/LED/led.h"
#include "./BSP/TIMER/btim.h"

int main(void)
{
    HAL_Init();                             /* 初始化HAL库 */
    sys_stm32_clock_init(336, 8, 2, 7);     /* 配置时钟，168MHz */
    delay_init(168);                        /* 初始化延时 */
    usart_init(115200);                     /* 初始化串口 */
    led_init();                             /* 初始化LED */
    btim_timx_int_init(10000 - 1, 8400);    /* 初始化基本定时器，溢出频率为1Hz ,1秒钟近一次中断*/
    
    while (1)
    {
        LED2_TOGGLE();
        delay_ms(200);
    }
}
