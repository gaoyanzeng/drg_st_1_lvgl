/*
 * Copyright 2023 NXP
 * NXP Confidential and Proprietary. This software is owned or controlled by NXP and may only be used strictly in
 * accordance with the applicable license terms. By expressly accepting such terms or by downloading, installing,
 * activating and/or otherwise using the software, you are agreeing that you have read, and that you agree to
 * comply with and are bound by, such license terms.  If you do not agree to be bound by the applicable license
 * terms, then you may not retain, install, activate or otherwise use the software.
 */
/*******************************************************************************
 * Size: 12 px
 * Bpp: 4
 * Opts: --user-data-dir=C:\Users\gao\AppData\Roaming\gui-guider --app-path=C:\NXP\GUI-Guider-1.6.1-GA\resources\app.asar --no-sandbox --no-zygote --lang=zh-CN --device-scale-factor=1 --num-raster-threads=4 --enable-main-frame-before-activation --renderer-client-id=5 --time-ticks-at-unix-epoch=-1700216787961469 --launch-time-ticks=2125888378 --mojo-platform-channel-handle=2904 --field-trial-handle=1672,i,6991791946442648325,16254591651633694587,131072 --disable-features=SpareRendererForSitePerProcess,WinRetrieveSuggestionsOnlyOnDemand /prefetch:1
 ******************************************************************************/

#ifdef LV_LVGL_H_INCLUDE_SIMPLE
#include "lvgl.h"
#else
#include "lvgl.h"
#endif

#ifndef LV_CUSTOMER_FONT_SIMSUN_12
#define LV_CUSTOMER_FONT_SIMSUN_12 1
#endif

#if LV_CUSTOMER_FONT_SIMSUN_12

/*-----------------
 *    BITMAPS
 *----------------*/

/*Store the image of the glyphs*/
static LV_ATTRIBUTE_LARGE_CONST const uint8_t glyph_bitmap[] = {
    /* U+535A "博" */
    0xf, 0x0, 0x0, 0xf0, 0xf, 0x0, 0xf, 0xf,
    0xff, 0xff, 0xff, 0xf0, 0xf, 0x0, 0xf0, 0xf0,
    0xf, 0x0, 0xff, 0xf0, 0xff, 0xff, 0xff, 0x0,
    0xf, 0x0, 0xf0, 0xf0, 0xf, 0x0, 0xf, 0x0,
    0xff, 0xff, 0xff, 0x0, 0xf, 0x0, 0xf0, 0xf0,
    0xf, 0x0, 0xf, 0x0, 0x0, 0x0, 0xf0, 0x0,
    0xf, 0xf, 0xff, 0xff, 0xff, 0xf0, 0xf, 0x0,
    0xf0, 0x0, 0xf0, 0x0, 0xf, 0x0, 0xf, 0xf,
    0xf0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,

    /* U+58EB "士" */
    0x0, 0x0, 0xf, 0x0, 0x0, 0x0, 0x0, 0x0,
    0xf, 0x0, 0x0, 0x0, 0x0, 0x0, 0xf, 0x0,
    0x0, 0x0, 0x0, 0x0, 0xf, 0x0, 0x0, 0x0,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xf0, 0x0, 0x0,
    0xf, 0x0, 0x0, 0x0, 0x0, 0x0, 0xf, 0x0,
    0x0, 0x0, 0x0, 0x0, 0xf, 0x0, 0x0, 0x0,
    0x0, 0x0, 0xf, 0x0, 0x0, 0x0, 0x0, 0x0,
    0xf, 0x0, 0x0, 0x0, 0xf, 0xff, 0xff, 0xff,
    0xff, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,

    /* U+9AD8 "高" */
    0x0, 0x0, 0xf, 0x0, 0x0, 0x0, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xf0, 0x0, 0x0, 0x0, 0x0,
    0x0, 0x0, 0x0, 0xf, 0xff, 0xff, 0xf0, 0x0,
    0x0, 0xf, 0x0, 0x0, 0xf0, 0x0, 0xf, 0xff,
    0xff, 0xff, 0xff, 0xf0, 0xf, 0x0, 0x0, 0x0,
    0x0, 0xf0, 0xf, 0xf, 0xff, 0xff, 0x0, 0xf0,
    0xf, 0xf, 0x0, 0xf, 0x0, 0xf0, 0xf, 0xf,
    0xff, 0xff, 0x0, 0xf0, 0xf, 0x0, 0x0, 0x0,
    0xff, 0xf0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0
};


/*---------------------
 *  GLYPH DESCRIPTION
 *--------------------*/

static const lv_font_fmt_txt_glyph_dsc_t glyph_dsc[] = {
    {.bitmap_index = 0, .adv_w = 0, .box_w = 0, .box_h = 0, .ofs_x = 0, .ofs_y = 0} /* id = 0 reserved */,
    {.bitmap_index = 0, .adv_w = 192, .box_w = 12, .box_h = 12, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 72, .adv_w = 192, .box_w = 12, .box_h = 12, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 144, .adv_w = 192, .box_w = 12, .box_h = 12, .ofs_x = 0, .ofs_y = -2}
};

/*---------------------
 *  CHARACTER MAPPING
 *--------------------*/

static const uint16_t unicode_list_0[] = {
    0x0, 0x591, 0x477e
};

/*Collect the unicode lists and glyph_id offsets*/
static const lv_font_fmt_txt_cmap_t cmaps[] =
{
    {
        .range_start = 21338, .range_length = 18303, .glyph_id_start = 1,
        .unicode_list = unicode_list_0, .glyph_id_ofs_list = NULL, .list_length = 3, .type = LV_FONT_FMT_TXT_CMAP_SPARSE_TINY
    }
};



/*--------------------
 *  ALL CUSTOM DATA
 *--------------------*/

#if LV_VERSION_CHECK(8, 0, 0)
/*Store all the custom data of the font*/
static  lv_font_fmt_txt_glyph_cache_t cache;
static const lv_font_fmt_txt_dsc_t font_dsc = {
#else
static lv_font_fmt_txt_dsc_t font_dsc = {
#endif
    .glyph_bitmap = glyph_bitmap,
    .glyph_dsc = glyph_dsc,
    .cmaps = cmaps,
    .kern_dsc = NULL,
    .kern_scale = 0,
    .cmap_num = 1,
    .bpp = 4,
    .kern_classes = 0,
    .bitmap_format = 0,
#if LV_VERSION_CHECK(8, 0, 0)
    .cache = &cache
#endif
};


/*-----------------
 *  PUBLIC FONT
 *----------------*/

/*Initialize a public general font descriptor*/
#if LV_VERSION_CHECK(8, 0, 0)
const lv_font_t lv_customer_font_simsun_12 = {
#else
lv_font_t lv_customer_font_simsun_12 = {
#endif
    .get_glyph_dsc = lv_font_get_glyph_dsc_fmt_txt,    /*Function pointer to get glyph's data*/
    .get_glyph_bitmap = lv_font_get_bitmap_fmt_txt,    /*Function pointer to get glyph's bitmap*/
    .line_height = 12,          /*The maximum line height required by the font  default: (f.src.ascent - f.src.descent)*/
    .base_line = 1.7999999999999998,                          /*Baseline measured from the bottom of the line*/
#if !(LVGL_VERSION_MAJOR == 6 && LVGL_VERSION_MINOR == 0)
    .subpx = LV_FONT_SUBPX_NONE,
#endif
#if LV_VERSION_CHECK(7, 4, 0) || LVGL_VERSION_MAJOR >= 8
    .underline_position = -1,
    .underline_thickness = 1,
#endif
    .dsc = &font_dsc           /*The custom font data. Will be accessed by `get_glyph_bitmap/dsc` */
};



#endif /*#if LV_CUSTOMER_FONT_SIMSUN_12*/

