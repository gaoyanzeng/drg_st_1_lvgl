/**
 ****************************************************************************************************
 * @file        main.c
 * @author      Mr.Mr.
 * @version     V1.0
 * @date        2023-04-23
 * @brief       触摸屏实验
 * @license     Copyright (c) 2020-2032,  
 ****************************************************************************************************
 * @attention
 * 
 * 实验平台:ST-1 STM32F407核心板
 *  
 *  
 * 公司网址:www.genbotter.com
 * 购买地址:makerbase.taobao.com
 * 
 ****************************************************************************************************
 */

#include "./SYSTEM/sys/sys.h"
#include "./SYSTEM/delay/delay.h"
#include "./SYSTEM/usart/usart.h"
#include "./BSP/LED/led.h"
#include "./BSP/KEY/key.h"
#include "./BSP/LCD/lcd.h"
#include "./BSP/TOUCH/touch.h"
#include "./BSP/SRAM/sram.h"
#include "./BSP/TIMER/btim.h"
#include "lvgl.h"
#include "lv_port_disp.h"
#include "lv_port_indev.h"

static void btn_event_handler(lv_event_t * e){
    static bool led_flg = false;
    LED2_TOGGLE();
    led_flg = !led_flg;
    
    lv_obj_t * btn = lv_event_get_target(e);
    lv_obj_t * label = lv_obj_get_child(btn, 0);
    
    if(led_flg){
        lv_label_set_text(label, "Turn OFF LED2");
    }else{
        lv_label_set_text(label, "Turn ON LED2");
    }
}

int main(void)
{
    HAL_Init();                         /* 初始化HAL库 */
    sys_stm32_clock_init(336, 8, 2, 7); /* 配置时钟，168MHz */
    delay_init(168);                    /* 初始化延时 */
    usart_init(115200);                 /* 初始化串口 */
    led_init();                         /* 初始化LED */
    key_init();                         /* 初始化按键 */
    sram_init();
//    lcd_init();                         /* 初始化LCD */
//    tp_dev.init();                      /* 初始化触摸屏 */
    
    btim_timx_int_init(1000 - 1, 84);   // 1ms中断初始化，重装载值、预分频值
    
    lv_init();                          // lvgl初始化
    lv_port_disp_init();                // 显示设备初始化
    lv_port_indev_init();               // 输入设备初始化
    
    /* 在屏幕上创建一个btn对象并居中显示 */
    lv_obj_t * btn_led2_obj = lv_btn_create(lv_scr_act());
    lv_obj_add_event_cb(btn_led2_obj, btn_event_handler, LV_EVENT_CLICKED, NULL);
    lv_obj_set_size(btn_led2_obj, 160, 30);
    lv_obj_align(btn_led2_obj, LV_ALIGN_CENTER, 0, 0);
    
    lv_obj_t * label_obj = lv_label_create(btn_led2_obj);
    lv_label_set_text(label_obj, "Turn ON LED2");
    lv_obj_center(label_obj);
    
    while(1){
        delay_ms(5);
        lv_timer_handler();
    }
}
